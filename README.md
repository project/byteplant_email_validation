## SUMMARY

This module uses the Byteplant(https://www.email-validator.net/) service to verify if an email address entered into a
form on your website really exists and can receive mails.

For each email invalid address you get a detailed validation result with the exact status.

## REQUIREMENTS

* None. (Other than a clean Drupal 8 installation)

## INSTALLATION

* Install as usual.
* Place the entirety of this directory in the /modules/contrib folder of your Drupal installation.
* Navigate to Administer > Extend.
* Check the 'Enabled' box next to the 'Byteplant email validation'.
* Click the 'Save Configuration' button at the bottom.

## CONFIGURATION

* Go to admin/config/development/email-validation.
* Set key from (https://www.email-validator.net/) and the forms id where you
want to apply the validations.
* If you want custom error message instead of Byteplant status message please
fill "Custom invalid email message".
* Add any email addresses or domain patterns that you would like to either allow
to bypass validation or automatically deny, without a call to byteplant. _This
can save credits if your site is being hit by spammers from a specific domain or
domain pattern._
* Select any debugging and logging options to help track down issues during the
validation process or to check that it's working as expected.

## CONTACT

### Current Maintainer

merauluka - https://www.drupal.org/u/merauluka

### Initial Maintainer

benellefimostfa - https://www.drupal.org/u/benellefimostfa

#### This project was originally sponsored by

Amazeelabs - https://www.drupal.org/amazee-labs
