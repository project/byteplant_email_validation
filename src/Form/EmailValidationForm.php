<?php

namespace Drupal\byteplant_email_validation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EmailValidationForm.
 *
 * @package Drupal\byteplant_email_validation\Form
 */
class EmailValidationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'byteplant_email_validation.byteplant_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'byteplant_key';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('byteplant_email_validation.byteplant_settings');
    $default_url = 'https://api.email-validator.net/api/verify';
    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BytePlant key'),
      '#description' => $this->t('Your byte plant key.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('key'),
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('BytePlant url'),
      '#description' => $this->t('Your byte plant url.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => ($config->get('url')) ? $config->get('url') : $default_url,
    ];

    $form['forms'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Forms to validate'),
      '#description' => $this->t('Forms where you want to apply the email validation, please insert form id per line.'),
      '#default_value' => $config->get('forms'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom invalid email message'),
      '#description' => $this->t('A static custom message that will be showed if the email is invalid.
      Leave blank to show  dynamic validation details provided by byteplant.'),
      '#default_value' => $config->get('message'),
    ];

    $form['bypass_options_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Bypass Options'),
      'bypass_description' => [
        '#type' => 'markup',
        '#markup' => $this->t('Use the allow and deny lists below to bypass byteplant email validation. This can be useful for trusted domains that are returning errors for some reason. Additionally they deny list allows administrators to prevent registrations from selected domains or email addresses.'),
        '#prefix' => '<div class="form-item__description">',
        '#suffix' => '</div>',
      ],
      'allow_list' => [
        '#type' => 'textarea',
        '#title' => $this->t('Allow list'),
        '#description' => $this->t('<p>Provide a list of email addresses, one per line, to pass through. All emails to addresses from this list will not be validated. <em>Only use with trusted addresses and domains.</em></p><strong>Examples:</strong><ul><li><code>*@example.com</code> &mdash; allow all email addresses from "example.com" to bypass validation.</li><li><code>myname*@example.com</code> &mdash; allow all email addresses from "example.com" that start with "myname" to bypass validation.</li></ul>'),
        '#default_value' => $config->get('allow_list'),
      ],
      'deny_list' => [
        '#type' => 'textarea',
        '#title' => $this->t('Deny list'),
        '#description' => $this->t('<p>Provide a list of email addresses, one per line, to automatically deny without validation. All emails to addresses from this list will not be validated. <em>It is better to choose more restrictive rules for deny lists to prevent unintentionally denying access.</em></p>
        <strong>Examples:</strong>
        <ul>
          <li><code>*@gmail.com</code> &mdash; deny all email addresses from "gmail.com".</li>
          <li><code>myname*@gmail.com</code> &mdash; deny all email addresses from "gmail.com" that start with "myname".</li>
          <li><code>myname*@gmail*</code> &mdash; deny all email addresses with a domain that begins with "gmail" that start with "myname".</li>
        </ul>'),
        '#default_value' => $config->get('deny_list'),
      ],
    ];

    $form['logging_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Logging and Debug Options'),
      'logging' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Select events to log during the email validation process:'),
        '#options' => [
          'success' => $this->t('Successful validations'),
          'error' => $this->t('Validation errors'),
          'request' => $this->t('Validation request process (about to send, complete, incomplete, etc)'),
          'bypass_allow' => $this->t('An email matches the Allow List'),
          'bypass_deny' => $this->t('An email matches the Deny List'),
          'email_error' => $this->t('Send email to default site email address on validation failure'),
          'email_success' => $this->t('Send email to default site email address on successful validation'),
          'email_bypass_allow' => $this->t('Send email to default site email address when an email matches the Allow List'),
          'email_bypass_deny' => $this->t('Send email to default site email address when an email matches the Deny List'),
        ],
        '#default_value' => $config->get('logging'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('byteplant_email_validation.byteplant_settings')
      ->set('key', $form_state->getValue('key'))
      ->set('url', $form_state->getValue('url'))
      ->set('forms', $form_state->getValue('forms'))
      ->set('message', $form_state->getValue('message'))
      ->set('allow_list', $form_state->getValue('allow_list'))
      ->set('deny_list', $form_state->getValue('deny_list'))
      ->set('logging', $form_state->getValue('logging'))
      ->save();
  }

}
