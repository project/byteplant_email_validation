<?php

namespace Drupal\byteplant_email_validation;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

/**
 * Class EmailValidationService.
 *
 * @package Drupal\byteplant_email_validation
 */
class EmailValidationService {

  /**
   * The byteplant_email_validation.byteplant_settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The Byteplant Email Validation logger.
   *
   * @var Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The array of configured events that should produce logs.
   *
   * @var array
   */
  protected $loggingOptions;

  /**
   * Constructs an EmailValidation object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \GuzzleHttp\Client $http_client
   *   The guzzle http client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Byteplant Email Validation logger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Client $http_client, LoggerInterface $logger) {
    $this->config = $config_factory->get('byteplant_email_validation.byteplant_settings');
    $this->httpClient = $http_client;
    $this->logger = $logger;
    $this->loggingOptions = array_filter($this->config->get('logging'));
  }

  /**
   * Get request using guzzle client.
   *
   * @return object
   *   The returned response from Byteplant or an empty object on error.
   */
  public function verifyEmail($email) {
    // Get Byteplant Key and url by form.
    $key = $this->config->get('key');
    $url = $this->config->get('url');
    $query = 'EmailAddress=' . $email . '&APIKey=' . $key;
    $url = $url . '?' . $query;
    if (in_array('request', $this->loggingOptions)) {
      $this->logger->info('The email @email is being submitted for validation.', ['@email' => $email]);
    }
    $response = $this->getRequest($url);
    if (in_array('request', $this->loggingOptions)) {
      $this->logger->info('The email @email was successfully submitted for validation.', ['@email' => $email]);
    }
    return $response;
  }

  /**
   * Get invalid email message to display on user forms.
   *
   * @return string
   *   The error message to display to the end user.
   */
  public function getMessage($content) {
    // If user set message then use it, else use byteplant status.
    if ($message = $this->config->get('message')) {
      return $message;
    }
    return $content->info . ': ' . $content->details;
  }

  /**
   * Get Email validation details from BytePlant service.
   *
   * @param string $url
   *   The url to use when making the request to byteplant.
   * @param array $options
   *   The array of options to include with the HTTP request to byteplant.
   *
   * @return object
   *   The returned response from Byteplant or an empty object on error.
   */
  public function getRequest(string $url, array $options = []) {
    try {
      $client = $this->httpClient;
      $request = $client->get($url, $options);
      $content = $request->getBody()->getContents();
      $content = json_decode($content);
    }
    catch (\Exception $e) {
      watchdog_exception('byteplant_email_validation', $e);
      $content = new \stdClass();
    }
    return $content;
  }

}
